class LineItemsController < ApplicationController
  include CurrentCart
  skip_before_action :authorize, only: [:create, :update, :destroy, :set_quantity ]
  before_action :current_cart, only: [:create]
  before_action :set_line_item, only: [:show, :edit, :update, :destroy, :set_quantity]

  # GET /line_items
  # GET /line_items.json
  def index
    @line_items = LineItem.all
  end

  # GET /line_items/1
  # GET /line_items/1.json
  def show
  end

  # GET /line_items/new
  def new
    @line_item = LineItem.new
  end

  # GET /line_items/1/edit
  def edit
  end

  # POST /line_items
  # POST /line_items.json
  def create
    product = Product.find(params[:product_id])
    @line_item = @cart.add_product(product.id)

    respond_to do |format|
      if @line_item.save
        session[:counter] = 0
        format.html { redirect_to store_url}
        format.js { @current_item = @line_item }
        format.json { render :show, status: :created, location: @line_item }
      else
        format.html { render :new }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /line_items/1
  # PATCH/PUT /line_items/1.json
  def update
    respond_to do |format|
      if @line_item.update(line_item_params)
        format.html { redirect_to @line_item, notice: 'Line item was successfully updated.' }
        format.json { render :show, status: :ok, location: @line_item }
      else
        format.html { render :edit }
        format.json { render json: @line_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /line_items/1
  # DELETE /line_items/1.json
  def destroy
    @cart = current_cart
    @line_item.destroy
    respond_to do |format|
      format.html { if (@cart.line_items.empty?)
        @cart.destroy
        redirect_to store_url, notice: "Your cart is empty"
      else
        redirect_to store_url, notice: 'Line item was successfully destroyed.'
      end }
      format.json { head :no_content }
    end
  end

  def set_quantity
    @cart = current_cart
    @line_item.update_column(:quantity, params[:line_item][:quantity])
    if params[:line_item][:quantity].to_i.zero?
      @line_item.destroy
    end
    redirect_to store_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_line_item
      @line_item = LineItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def line_item_params
      params.require(:line_item).permit(:product_id)
    end
end
