class Product < ActiveRecord::Base
  has_many :line_items
  has_many :orders, through: :line_items
  before_destroy :ensure_not_referenced_by_any_line_item

  has_attached_file :image_url, :styles => {:medium => "300x300>", :thumb => "60x70>"}
  validates_attachment :image_url, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }

  validates :title, :description, :image_url, presence: true
  validates :price, numericality: {greater_than_or_equal_to: 0.01}
  validates :title, uniqueness: true
  #VALID_IMAGE_REGEX = /\w+\.(gif|jpg|png)\z/i
  #validates :image_url, format: { with: VALID_IMAGE_REGEX, message: 'must be a URL for GIF, JPG or PNG image.' } 
  validates_length_of :description, minimum: 5, message: "Please enter at least 5 characters."

  def self.latest
    Product.order(:updated_at).last
  end

  # ensure that there are no line items referencing this product
  def ensure_not_referenced_by_any_line_item
    if line_items.empty?
      return true
    else
      errors.add(:base, 'Line Items present')
      return false
    end
  end

end
