class StoreController < ApplicationController
  skip_before_action :authorize
  include CurrentCart
  before_action :current_cart
  def index
    @products = Product.order(:title)
    @counter = session[:counter]
    @counter.nil? ? @counter = 1 : @counter+=1 ;
    if @counter > 5
      @session_msg = "This page has been visited " + @counter.to_s + " times."
    else
      @session_msg = ""
    end
    session[:counter] = @counter
  end
end
